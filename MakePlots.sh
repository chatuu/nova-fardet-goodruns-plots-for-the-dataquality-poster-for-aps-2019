#!/bin/sh

for i in 0 1 2
do

root -l -b -q "PlotDB_Web.C($i)"
root -l -b -q "PlotEfficiency.C($i)"
root -l -b -q "GoodDataSelDate.C($i)"
root -l -b -q "GoodDataSelLiveTime.C($i)"
root -l -b -q "GoodDataSelRuns.C($i)" 
root -l -b -q "GoodDataSelGoodDB.C($i)" 
root -l -b -q "GoodDataSelMipRate.C($i)" 
root -l -b -q "GoodDataSelNumSlices.C($i)" 
root -l -b -q "GoodDataSelTrkFrac.C($i)" 

done

